const Sentry = require('@sentry/node');
Sentry.init({
  dsn: 'https://22bd6272633744f8b5a950389c37f2aa@sentry.io/3056747',
  environment: strapi.config.environment,
});

module.exports = strapi => {
  return {
    initialize() {
      strapi.app.use(async (ctx, next) => {
        try {
          await next();
        } catch (error) {
          Sentry.captureException(error);
          throw error;
        }
      });
    },
  };
};
