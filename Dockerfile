FROM node:lts-alpine as base
WORKDIR /usr/src/app/
COPY package.json /usr/src/app/
RUN npm install --production
COPY config /usr/src/app/config
ENV NODE_OPTIONS="--max-old-space-size=8192"
RUN npm run build
EXPOSE 1337

FROM base as production
ENV NODE_ENV=production
COPY . .
CMD npm run start

FROM base as develop
ENV NODE_ENV=development
RUN npm install --only=dev
CMD npm run develop