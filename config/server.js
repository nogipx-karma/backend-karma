module.exports = ({ env }) => ({
  host: "0.0.0.0",
  port: "1337",
  url: `https://api.nogipx.ru`,
  cron: {
    enabled: false
  },
  admin: {
    autoOpen: false
  }
});
