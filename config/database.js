module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: env("DB_CONNECTOR", "mongoose"),
      settings: {
        client: env("DB_CLIENT", 'mongo'),
        host: env("DB_HOST", 'localhost'),
        port: env("DB_PORT", 27017),
        database: env("DB_NAME", 'strapi_dev'),
        username: env("DB_USERNAME", 'root'),
        password: env("DB_PASSWORD", '')
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
