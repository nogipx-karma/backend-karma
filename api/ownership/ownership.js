const { sanitizeEntity } = require('strapi-utils')

module.exports = {
  find: contentType => async ctx => {
    let results = strapi.models[contentType].find({
      owner: ctx.state.user.id
    });
    if (results.length > 0) {
      return results.map(entity => sanitizeEntity(entity, {
        model: strapi.models[contentType]
      }))
    }
    return results
  },
  findOne: contentType => async ctx => {
    let result = await strapi.query(contentType).model.find({
      owner: ctx.state.user.id, _id: ctx.params.id
    });
    return sanitizeEntity(result["0"], { model: strapi.models[contentType] });
  },

  count: contentType => async ctx => strapi
    .query(contentType).model
    .find({ owner: ctx.state.user.id })
    .countDocuments(),

  create: contentType => async ctx => {
    ctx.request.body.owner = ctx.state.user.id
    const result = await strapi.models[contentType].create(ctx.request.body);
    return sanitizeEntity(result, { model: strapi.models[contentType] })
  },

  update: contentType => async ctx => {
    let result = await strapi.models[contentType].findOneAndUpdate({
      owner: ctx.state.user.id, _id: ctx.params.id
    }, ctx.request.body, { new: true });
    return sanitizeEntity(result, { model: strapi.models[contentType] })
  },

  delete: contentType => async ctx => strapi
    .query(contentType)
    .delete({
      owner: ctx.state.user.id, _id: ctx.params.id
    })
}
